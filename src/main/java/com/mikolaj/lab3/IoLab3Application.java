package com.mikolaj.lab3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IoLab3Application {

	public static void main(String[] args) {
		SpringApplication.run(IoLab3Application.class, args);
		System.out.println("Hello World");
	}

}
